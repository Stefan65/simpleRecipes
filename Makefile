TMPFILES = *.log *.aux *.toc *.out *.lof *.lot *.snm *.nav *.vrb *.bak *.gz *.idx

.PHONY: default
default:	all

.PHONY: all
all:		package \
		docs

package:	simpleRecipes.ins_to_sty
docs:		simpleRecipes.dtx_to_pdf

%.ins_to_sty: %.ins
	latex $<

%.dtx_to_pdf: %.dtx
	pdflatex $<
	pdflatex -interaction=batchmode $<
	pdflatex -interaction=batchmode $<

.PHONY: clean
clean:
	rm -f $(TMPFILES)
	rm -f *.sty
	rm -f *.pdf
