# simpleRecipes - LaTeX package for simple recipes

LaTeX package to typeset recipes in a basic manner without any fancy styles.

## Usage

* Clone the repository.
* Run `make all` to extract the package source and create the documentation.
* Copy at least the `simpleRecipes.sty` file to a directory searched by TeX, for example `~/texmf/tex/simpleRecipes/simpleRecipes.sty`.

## License

```
Copyright (C) 2016-2020 by Stefan65

This file may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either
version 1.3 of this license or (at your option) any later
version. The latest version of this license is in:

   http://www.latex-project.org/lppl.txt

and version 1.3 or later is part of all distributions of
LaTeX version 2005/12/01 or later.
```
